﻿using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Actions.Base;
using Delphic.Platform.Sc.Rules.Utils;
using Sitecore;
using Sitecore.Data.Items;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Actions.BranchDatasources
{
    [UsedImplicitly]
    [Guid("8E804F7A-77FF-4074-9BAC-7EEE0CFA89C6")]
    public sealed class RelinkDescendantsAndSelfBranchDatasources<TRuleContext> : RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            BranchDatasourceUtils.RelinkDatasourcesInBranchInstance(ruleContext.Item, true);
        }
    }
}
