﻿using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Actions.Base;
using Delphic.Platform.Sc.Rules.Utils;
using Sitecore;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Actions.BranchDatasources
{
    [UsedImplicitly]
    [Guid("4B439446-C6FD-4635-8282-147CA80A4CA5")]
    public sealed class RelinkItemBranchDatasources<TRuleContext> : RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            BranchDatasourceUtils.RelinkDatasourcesInBranchInstance(ruleContext.Item);
        }
    }
}
