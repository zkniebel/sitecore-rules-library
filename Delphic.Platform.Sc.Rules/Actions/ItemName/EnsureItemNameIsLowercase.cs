﻿using System;
using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Actions.Base;
using Sitecore;
using Sitecore.Data.Events;
using Sitecore.Data.Items;
using Sitecore.Rules;
using Sitecore.SecurityModel;

namespace Delphic.Platform.Sc.Rules.Actions.ItemName
{
    /// <summary>
    /// Rules engine action to lowercase item names.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Type providing rule context.
    /// </typeparam>
    /// <credit>
    /// The original code and idea is from <seealso href="http://marketplace.sitecore.net/en/Modules/Item_Naming_rules.aspx"/></credit>
    [UsedImplicitly]
    [Guid("2202ED0C-F4AC-4229-8E1B-8FA4C2BF827C")]
    public sealed class EnsureItemNameIsLowercase<TRuleContext> : RuleAction<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The apply rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        protected override void ApplyRule(TRuleContext ruleContext)
        {
            var newName = ruleContext.Item.Name.ToLowerInvariant();

            if (ruleContext.Item.Name == newName)
            {
                return;
            }

            newName = ItemUtil.ProposeValidItemName(newName);

            using (new SecurityDisabler())
            {
                using (new EditContext(ruleContext.Item))
                {
                    using (new EventDisabler())
                    {
                        ruleContext.Item.Name = newName;
                    }
                }
            }
        }
    }
}
