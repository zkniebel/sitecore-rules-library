﻿using System.Runtime.InteropServices;
using Delphic.Platform.Sc.Rules.Conditions.Base;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Diagnostics;
using Sitecore.Rules;

namespace Delphic.Platform.Sc.Rules.Conditions.ItemInformation
{
    /// <summary>
    /// Rule that walks the supplied Item's descendants' template inheritance hierarchy looking
    /// for a match to the TemplateID provided by the rule context.
    /// </summary>
    /// <typeparam name="TRuleContext">
    /// Instance of Sitecore.Rules.Conditions.RuleContext.
    /// </typeparam>
    [UsedImplicitly]
    [Guid("AB9C0919-7615-4D06-A687-D84936419193")]
    public sealed class DescendantInheritsFromTemplate<TRuleContext> : WhenCondition<TRuleContext>
        where TRuleContext : RuleContext
    {
        /// <summary>
        /// The backing field for the template Id.
        /// </summary>
        private ID _templateId;

        /// <summary>
        /// Gets or sets the template id.
        /// </summary>
        [NotNull]
        public ID TemplateId
        {
            get
            {
                return _templateId;
            }

            set
            {
                Assert.ArgumentNotNull(value, nameof(value));
                _templateId = value;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DescendantInheritsFromTemplate{TRuleContext}"/> class. 
        /// </summary>
        public DescendantInheritsFromTemplate()
        {
            _templateId = ID.Null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DescendantInheritsFromTemplate{TRuleContext}"/> class. 
        /// </summary>
        /// <param name="templateId">
        /// The template id.
        /// </param>
        public DescendantInheritsFromTemplate([NotNull] ID templateId)
        {
            Assert.ArgumentNotNull(templateId, nameof(templateId));
            _templateId = templateId;
        }

        /// <summary>
        /// The execute rule.
        /// </summary>
        /// <param name="ruleContext">
        /// The rule context.
        /// </param>
        /// <returns>
        /// <c>True</c>, if the condition succeeds, otherwise <c>false</c>.
        /// </returns>
        protected override bool ExecuteRule(TRuleContext ruleContext)
        {
            var item = ruleContext.Item;
            return item != null && ProcessDescendants(item);
        }

        private bool ProcessDescendants(Item item)
        {
            foreach (Item child in item.Children)
            {
                if (child.TemplateID == TemplateId)
                {
                    return true;
                }

                var template = TemplateManager.GetTemplate(child);
                if (template != null && template.InheritsFrom(TemplateId))
                {
                    return true;
                }

                if (ProcessDescendants(child))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
